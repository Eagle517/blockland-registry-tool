﻿using Microsoft.Win32;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BlocklandRegistryTool
{
	public partial class MainWindow : Window
	{
		private Brush invalidBrush = Brushes.Red;

		public MainWindow()
		{
			InitializeComponent();
		}

		private bool validateTextBox(TextBox tb, bool trim=true, string cmp="", bool isRegex=false)
		{
			string tbTxt = trim ? tb.Text.Trim() : tb.Text;
			bool invalid = isRegex ? !Regex.Match(tbTxt, cmp).Success : (tbTxt == cmp);

			if (invalid)
				tb.BorderBrush = invalidBrush;
			else
				tb.ClearValue(TextBox.BorderBrushProperty);
			
			return invalid;
		}

		private void blLocTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			validateTextBox(blLocTextBox);
		}

		private void blLocTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			validateTextBox(blLocTextBox);
		}

		private void blBLIDTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			validateTextBox(blBLIDTextBox, true, @"^[1-9][0-9]*$", true);
		}

		private void blBLIDTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			validateTextBox(blBLIDTextBox, true, @"^[1-9][0-9]*$", true);
		}

		private void webLocTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			validateTextBox(webLocTextBox);
		}

		private void webLocTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			validateTextBox(webLocTextBox);
		}

		private void blLocBrowseBtn_Click(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.OpenFileDialog dlg = new OpenFileDialog();
			dlg.DefaultExt = ".exe";
			dlg.Filter = "Blockland.exe (*.exe)|*.exe|All Files (*)|*";

			Nullable<bool> result = dlg.ShowDialog();

			if (result == true)
			{
				string filename = dlg.FileName;
				blLocTextBox.Text = filename;
			}
		}

		private void webLocBrowseBtn_Click(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.OpenFileDialog dlg = new OpenFileDialog();
			dlg.DefaultExt = ".exe";
			dlg.Filter = "Web Browser (*.exe)|*.exe";

			Nullable<bool> result = dlg.ShowDialog();

			if (result == true)
			{
				string filename = dlg.FileName;
				webLocTextBox.Text = filename;
			}
		}

		private void blSubmitBtn_Click(object sender, RoutedEventArgs e)
		{
			blFormResponse.Text = "";

			string path = blLocTextBox.Text;
			string blid = blBLIDTextBox.Text;
			string args = blArgsTextBox.Text.Trim();

			bool invalidForm = validateTextBox(blLocTextBox);
			invalidForm = validateTextBox(blBLIDTextBox, true, @"^[1-9][0-9]*$", true) || invalidForm;

			if (invalidForm)
				return;

			string command;
			if (args != "")
				command = $"\"{path}\" ptlaaxobimwroe {args} -blid {blid} %1";
			else
				command = $"\"{path}\" ptlaaxobimwroe -blid {blid} %1";

			try
			{
				Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\blockland", "", "URL:Blockland", RegistryValueKind.String);
				Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\blockland", "URL Protocol", "", RegistryValueKind.String);
				Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\blockland\\DefaultIcon", "", $"\"{path}\",-1", RegistryValueKind.String);
				Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\blockland\\shell\\open\\command", "", command, RegistryValueKind.String);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Blockland Registry Tool - Error", MessageBoxButton.OK, MessageBoxImage.Error);
				blFormResponse.Text = "Failed to register";
				return;
			}

			blFormResponse.Text = "Successfully registered";
		}

		private void webSubmitBtn_Click(object sender, RoutedEventArgs e)
		{
			webFormResponse.Text = "";

			string path = webLocTextBox.Text;
			string args = webArgsTextBox.Text;

			bool invalidForm = validateTextBox(webLocTextBox);

			if (invalidForm)
				return;

			string command = $"\"{path}\" \"%1\"";
			if (args != "")
				command += $" {args}";

			try
			{
				Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\http\\shell\\open\\command", "", command, RegistryValueKind.String);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Blockland Registry Tool - Error", MessageBoxButton.OK, MessageBoxImage.Error);
				webFormResponse.Text = "Failed to register";
				return;
			}

			webFormResponse.Text = "Successfully registered - restart Blockland for it to take effect";
		}
	}
}
