# Blockland Registry Tool
This simple tool lets you quickly configure the two registry settings Blockland uses:  the blockland:// protocol for joining via the web and which web browser Blockland uses when you open a link in-game.

Currently, there's a bug with how Blockland handles the blockland:// protocol.  As a result, you must specify which BLID you will use when you join a server via this protocol, even if you only have one BLID.
